# variables to be pre-filled for Run Pipeline must be at the top level since naturally they'll apply to all jobs.
variables:
  default_repo_name:
    description: "The slug of the repository that the cookiecutter will be deployed to."
  DEFAULT_NAMESPACE_COOKIECUT_TO:
    description: "The namespace (group or user name) containing the repository that the cookiecutter will be deployed to."
    # If we set a description without setting a value, the variable gets set to empty,
    # which screws with the logic of defaulting to CI_PROJECT_NAMESPACE since it's not clear whether the variable was deliberately set to empty.
    # Thus, the intermediate variable DEFAULT_NAMESPACE_COOKIECUT_TO.

.cookiecutter_deployment_before_script:
  before_script:
  - if [ -z ${NAMESPACES_COOKIECUT_TO+ABC} ]; then
  # If NAMESPACES_COOKIECUT_TO is set to empty, then we'll respect that, we might need that.
  # (We should probably avoid adding the / in that case.)
  # But we want to have a variable that is prompted for when you Run Pipeline but can be left blank.
  # That's DEFAULT_NAMESPACE_COOKIECUT_TO.
  - if [ -z ${DEFAULT_NAMESPACE_COOKIECUT_TO} ]; then
  - NAMESPACES_COOKIECUT_TO=$CI_PROJECT_NAMESPACE
  - else
  - NAMESPACES_COOKIECUT_TO=$DEFAULT_NAMESPACE_COOKIECUT_TO
  - fi
  - echo "NAMESPACES_COOKIECUT_TO was not set, so using $NAMESPACES_COOKIECUT_TO"
  - fi

  - if [ -z ${project_name+ABC} ]; then
  - project_name=$default_project_name
  - fi
  - if [ -z ${repo_name+ABC} ] && ! [ -z ${default_repo_name} ]; then
  - repo_name=$default_repo_name
  - fi
  - if [ -z ${repo_name+ABC} ] && ! [ -z ${project_name+ABC} ]; then
  # If repo_name is undefined and project_name is defined, use project_name to define repo_name.
  # This python- thing is only really appropriate for the Python cookiecutter.
  - repo_name=python-$(echo "$project_name" | tr '[:upper:]' '[:lower:]' | sed 's/ /-/g')
  - fi

.cookiecutter_deployment_base:
  variables:
    # These variables can be overridden by setting them on the project.
    # https://docs.gitlab.com/ee/ci/variables/#priority-of-cicd-variables
    COOKIECUTTER_CONTAINER_IMAGE_PREFIX: $CI_REGISTRY/
    COOKIECUTTER_CONTAINER_IMAGE_NAMESPACE: pythonpackagesalpine
    COOKIECUTTER_CONTAINER_IMAGE_NAME: cookiecutter-alpine
    COOKIECUTTER_CONTAINER_IMAGE_TAG: cookiepatcher-alpine
  image: ${COOKIECUTTER_CONTAINER_IMAGE_PREFIX}${COOKIECUTTER_CONTAINER_IMAGE_NAMESPACE}/${COOKIECUTTER_CONTAINER_IMAGE_NAME}:${COOKIECUTTER_CONTAINER_IMAGE_TAG}
  stage: build
  tags:
  - docker

.generate_ssh_private_deploy_key:
  rules:
  - if: '$SSH_PRIVATE_DEPLOY_KEY == null'
  extends: .cookiecutter_deployment_base
  image: alpine:edge
  script:
  - !reference [.cookiecutter_deployment_before_script, before_script]
  - echo "SSH_PRIVATE_DEPLOY_KEY is undefined. We cannot deploy the cookiecutter to the child repository without a deploy key."
  - echo "The public key needs to be given write permissions at https://$CI_SERVER_HOST/$NAMESPACES_COOKIECUT_TO/$repo_name/-/settings/repository"
  - echo "The private key needs to be set as SSH_PRIVATE_DEPLOY_KEY at $CI_PROJECT_URL/-/settings/ci_cd"
  - echo "If $CI_PROJECT_URL/edit shows that pipelines are currently not visible, great! We'll generate a deploy key for you. You can use the deploy key below, but remember that old pipelines can always become visible again, so make sure to use the red Delete button at $CI_PIPELINE_URL to delete this pipeline."
  # https://docs.gitlab.com/ee/api/pipelines.html#delete-a-pipeline
  # https://python-gitlab.readthedocs.io/en/stable/gl_objects/pipelines_and_jobs.html
  # but even if we could delete the pipeline from here, user needs to get the key first
  - echo "If $CI_PROJECT_URL/edit shows that pipelines are currently visible, do not use the deploy key generated below."
  - echo $CI_PROJECT_VISIBILITY
  - apk add openssh-keygen
  - ssh-keygen -t ed25519 -C "Key for deploying $CI_PROJECT_TITLE to child repo." -f id_ed25519 -N ""
  # https://www.ssh.com/ssh/passphrase
  # In practice, however, most SSH keys are without a passphrase. There is no human to type in something for keys used for automation. The passphrase would have to be hard-coded in a script or stored in some kind of vault, where it can be retrieved by a script.
  - echo "Paste $(cat id_ed25519.pub) into https://$CI_SERVER_HOST/$NAMESPACES_COOKIECUT_TO/$repo_name/-/settings/repository under Deploy Keys."
  - echo "Paste the key below into $CI_PROJECT_URL/-/settings/ci_cd with the name SSH_PRIVATE_DEPLOY_KEY."
  - cat id_ed25519
  - rm id_ed25519
  - echo "AND THEN CLICK THE RED DELETE BUTTON AT $CI_PIPELINE_URL TO DELETE THIS PIPELINE!"

.deploy_cookiecutter_to_child_repo:
  rules:
  - if: '$SSH_PRIVATE_DEPLOY_KEY != null'
  extends: .cookiecutter_deployment_base
  script:
  - !reference [.cookiecutter_deployment_before_script, before_script]
  # ssh-agent -s starts the ssh-agent and then outputs shell commands to run.
  - eval $(ssh-agent -s)

  ##
  ## Add the SSH key stored in SSH_PRIVATE_DEPLOY_KEY variable to the agent store.
  ## We're using tr to fix line endings which makes ed25519 keys work
  ## without extra base64 encoding.
  ## We use -d because the version of tr on alpine does not recognize --delete.
  ## https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
  ##
  - echo "$SSH_PRIVATE_DEPLOY_KEY" | tr -d '\r' | ssh-add -
  - echo "Added the private SSH deploy key with public fingerprint $(ssh-add -l)"

  # If we split up the git clone and git push into before_script and after_script,
  # the SSH key will be lost.

  # To successfully clone the child repo, we need to trust this instance of GitLab.
  - mkdir --parents ~/.ssh
  - ssh-keyscan $CI_SERVER_HOST >> ~/.ssh/known_hosts

  - echo $project_name
  - echo $repo_name
  - echo $NAMESPACES_COOKIECUT_TO
  - if ls set_variable_defaults.sh; then . set_variable_defaults.sh; fi

  - for NAMESPACE_COOKIECUT_TO in $NAMESPACES_COOKIECUT_TO; do
  - echo $NAMESPACE_COOKIECUT_TO

  - if [ -z ${REPO_COOKIECUT_TO+ABC} ]; then
  -   if [ -z ${repo_name+ABC} ]; then
  -     echo "REPO_COOKIECUT_TO and repo_name are both undefined, so we have nowhere to cookiecut to."; false
  -   fi # endif repo_name is undefined
  -   REPO_COOKIECUT_TO=git@$CI_SERVER_HOST:$NAMESPACE_COOKIECUT_TO/$repo_name.git
  - fi # endif REPO_COOKIECUT_TO is undefined
  - echo   "default_context:" > $HOME/.cookiecutterrc
  - if ! [ -z ${project_name+ABC} ]; then
  -   printf "  project_name:" >> $HOME/.cookiecutterrc
  -   echo " $project_name" >> $HOME/.cookiecutterrc
  - fi
  - if ! [ -z ${repo_name+ABC} ]; then
  -   printf "  repo_name:" >> $HOME/.cookiecutterrc
  -   echo " $repo_name" >> $HOME/.cookiecutterrc
  - fi
  # Unlike repo_name, NAMESPACE_COOKIECUT_TO is always defined.
  - printf "  repo_username:" >> $HOME/.cookiecutterrc
  - echo " $NAMESPACE_COOKIECUT_TO" >> $HOME/.cookiecutterrc
  - printf "  NAMESPACE_COOKIECUT_TO:" >> $HOME/.cookiecutterrc
  - echo " $NAMESPACE_COOKIECUT_TO" >> $HOME/.cookiecutterrc
  - printf "  cookiecutter_project_path_slug:" >> $HOME/.cookiecutterrc
  - echo " $CI_PROJECT_PATH" >> $HOME/.cookiecutterrc
  - printf "  branch_cookiecut_from:" >> $HOME/.cookiecutterrc
  - echo " $CI_COMMIT_REF_NAME" >> $HOME/.cookiecutterrc
  - printf "  repo_hosting_domain:" >> $HOME/.cookiecutterrc
  - echo " $CI_SERVER_HOST" >> $HOME/.cookiecutterrc
  - GITLAB_PAGES_COOKIECUT_TO=http://$NAMESPACE_COOKIECUT_TO.$CI_PAGES_DOMAIN/$(echo $repo_name | tr '[:upper:]' '[:lower:]')
  - printf "  GITLAB_PAGES_COOKIECUT_TO:" >> $HOME/.cookiecutterrc
  - echo " $GITLAB_PAGES_COOKIECUT_TO" >> $HOME/.cookiecutterrc
  - if ls add_to_cookiecutterrc_script.sh; then . add_to_cookiecutterrc_script.sh; fi

  - if [ -z ${COOKIECUT_TO_DEFAULT_BRANCH+ABC} ]; then
  -   if [ -z ${BRANCH_COOKIECUT_TO+ABC} ]; then
  -     BRANCH_COOKIECUT_TO=$CI_COMMIT_BRANCH
  -   fi # endif BRANCH_COOKIECUT_TO is undefined
  -   echo "We will cookiecut to $REPO_COOKIECUT_TO@$BRANCH_COOKIECUT_TO"
  - else
  -   echo "We will cookiecut to $REPO_COOKIECUT_TO"
  - fi

  # We cannot be sure that the branch BRANCH_COOKIECUT_TO already exists.
  - git clone $REPO_COOKIECUT_TO
  - if ! [ -z ${BRANCH_COOKIECUT_TO+ABC} ]; then
  -   cd $repo_name
  -   git checkout $BRANCH_COOKIECUT_TO || git checkout -b $BRANCH_COOKIECUT_TO
  -   cd ..
  - fi

  # If we don't export these variables, then they're only accessible in this shell,
  # in particular, Python's os.environ will not find them.
  - export NAMESPACE_COOKIECUT_TO
  - export repo_name

  - if ls cookiecut_script.sh; then . cookiecut_script.sh; else
  - git status
  - ls cookiecutter.json
  - if ls $repo_name/replay.yaml; then
  # Some things can only be done with a replay file.
  # For example, if one of the variables needs to be set to a list.
  - cookiecutter --overwrite-if-exists ./ --replay-file $repo_name/replay.yaml
  - else
  - if ls $repo_name/.cookiecutterrc; then
  # cookiepatcher automatically reads from the .cookiecutterrc in the target repo.
  - cookiepatcher ./ $repo_name --no-input
  # - cat $repo_name/.cookiecutterrc | sed 's/default_context/cookiecutter/' > $repo_name/replay.yaml
  - else
  - cookiecutter --overwrite-if-exists --no-input .
  - fi # if ls $repo_name/.cookiecutterrc else
  - fi # if ls $repo_name/replay.yaml else
  - fi # if ls cookiecut_script.sh else

  - cd $repo_name
  - git status
  - git diff
  - git add --all
  - git config user.email "$GITLAB_USER_EMAIL"
  - git config user.name "$GITLAB_USER_NAME"
  - git commit --allow-empty -m "Regenerated from cookiecutter. $CI_COMMIT_MESSAGE"
  - git push -u origin $BRANCH_COOKIECUT_TO
  - cd ..
  - rm -r -f $repo_name/.git/
  - rm -r $repo_name
  - unset REPO_COOKIECUT_TO

  - done


